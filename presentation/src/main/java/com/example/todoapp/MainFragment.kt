package com.example.todoapp

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.domain.model.CatalogModel
import com.example.kinopoisk.base.BaseFragment
import com.example.todoapp.catalog.list.CatalogAdapter
import com.example.todoapp.catalog.list.CatalogListViewModel
import com.example.todoapp.utils.Status
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject


@Suppress("DEPRECATION", "DEPRECATION")
class MainFragment : BaseFragment() {


    private lateinit var navController: NavController

    private lateinit var todayView: TextView
    private lateinit var favoriteView: TextView
    private lateinit var taskView: TextView

    private lateinit var addCatalog: ImageButton

    private val viewModel: CatalogListViewModel by inject()
    private lateinit var rvCatalogList: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    @SuppressLint("UseCompatLoadingForDrawables", "ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.loadCatalogList()
        bindViews(view)
        setAdapter()
        setData()

        setHasOptionsMenu(true)
        val toolbar = view.findViewById(R.id.main_toolbar) as Toolbar?
        toolbar?.inflateMenu(R.menu.options_menu)
        toolbar?.title = "ToDo App"


        addCatalog.setOnClickListener {
            viewModel.createCatalog()
        }

        todayView.setOnTouchListener { v, event ->
            todayView.setBackgroundColor(resources.getColor(R.color.darkGrey))
            return@setOnTouchListener false
        }

        favoriteView.setOnTouchListener { v, event ->
            favoriteView.setBackgroundColor(resources.getColor(R.color.darkGrey))
            return@setOnTouchListener false
        }

        taskView.setOnTouchListener { v, event ->
            taskView.setBackgroundColor(resources.getColor(R.color.darkGrey))
            return@setOnTouchListener false
        }

        todayView.setOnClickListener {
            val bundle = Bundle()
            bundle.putLong("id", 0)
            bundle.putString("catalogName", "Мой день")
            navController.navigate(
                    R.id.action_Main_to_TaskList,
                    bundle
            )
        }


        favoriteView.setOnClickListener {
            val bundle = Bundle()
            bundle.putLong("id", 0)
            bundle.putString("catalogName", "Важно")
            navController.navigate(
                    R.id.action_Main_to_TaskList,
                    bundle
            )
        }

        taskView.setOnClickListener {
            val bundle = Bundle()
            bundle.putLong("id", 0)
            bundle.putString("catalogName", "Задачи")
            navController.navigate(
                    R.id.action_Main_to_TaskList,
                    bundle
            )
        }


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.search){
            navController.navigate(
                R.id.action_Main_to_SearchFragment
            )
        }
        return super.onOptionsItemSelected(item)
    }


    private val onClickListener = object:
            CatalogAdapter.ItemClickListener {
        override fun onItemClick(item: CatalogModel) {
            val bundle = Bundle()
            bundle.putLong("id", item.id)
            bundle.putString("catalogName", item.name)
            navController.navigate(
                    R.id.action_Main_to_TaskList,
                    bundle
            )
        }

        override fun deleteCatalog(catalog: CatalogModel) {
            viewModel.deleteCatalog(catalog)
        }
    }
    private val catalogAdapter by lazy {
        CatalogAdapter(
                context = context,
                itemClickListener = onClickListener
        )
    }

     override fun bindViews(view: View) = with(view) {
         navController = Navigation.findNavController(this)
         rvCatalogList = findViewById(R.id.rvCatList)
         rvCatalogList.layoutManager = LinearLayoutManager(context)
         addCatalog = findViewById(R.id.addCatalog)
         todayView = findViewById(R.id.todayView)
         favoriteView = findViewById(R.id.favoriteView)
         taskView = findViewById(R.id.taskView)
    }

    override fun setData() {
        lifecycleScope.launch{
            val value = viewModel.uiState
            value.collect {
                when(it.status){
                    Status.LOADING -> {
                        Log.d("status", "loading")
                    }
                    Status.SUCCESS ->{
                        it.data.let {
                            catalogAdapter.setCatalogList(it!!)
                            Log.d("status", it.size.toString())
                        }
                    }
                    Status.UPDATE -> {
                        val bundle = Bundle()
                        it.message?.toLong()?.let { it1 -> bundle.putLong("id", it1) }
                        bundle.putString("catalogName", "")
                        navController.navigate(
                                R.id.action_Main_to_TaskList,
                                bundle
                        )
                    }
                    Status.DELETE -> {
                        viewModel.loadCatalogList()
                    }

                }
            }
        }
    }

    private fun setAdapter(){
        rvCatalogList.adapter = catalogAdapter
    }
}