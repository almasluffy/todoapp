package com.example.todoapp.di

import com.example.todoapp.catalog.create.CatalogCreateViewModel
import com.example.todoapp.catalog.list.CatalogListViewModel
import com.example.todoapp.tasks.detail.TaskDetailViewModel
import com.example.todoapp.tasks.TaskListViewModel
import com.example.todoapp.tasks.create.TaskCreateViewModel
import com.example.todoapp.tasks.search.SearchViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { TaskListViewModel(taskRepository = get()) }
    viewModel {  TaskDetailViewModel(taskRepository = get())}
    viewModel {  TaskCreateViewModel(taskRepository = get())}
    viewModel {  CatalogListViewModel(catalogRepository = get())}
    viewModel {  CatalogCreateViewModel(catalogRepository = get())}
    viewModel {  SearchViewModel(taskRepository = get())}
}