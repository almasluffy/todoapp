package com.example.kinopoisk.base

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.domain.model.TaskModel

abstract class BaseViewHolder(val view: View): RecyclerView.ViewHolder(view) {
    protected abstract fun clear()
}