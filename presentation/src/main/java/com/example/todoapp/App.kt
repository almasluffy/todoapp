package com.example.todoapp

import android.app.Application
import com.example.data.repositoryModule
import com.example.data.roomModule
import com.example.todoapp.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(listOf( roomModule, repositoryModule, viewModelModule)
            )
        }
    }
}