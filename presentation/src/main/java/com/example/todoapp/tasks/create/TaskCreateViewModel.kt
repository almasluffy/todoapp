package com.example.todoapp.tasks.create

import android.util.Log
import com.example.domain.model.TaskModel
import com.example.domain.repository.TaskRepository
import com.example.kinopoisk.base.BaseViewModel
import com.example.todoapp.utils.Resource
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import java.text.SimpleDateFormat
import java.util.*

class TaskCreateViewModel(private val taskRepository: TaskRepository): BaseViewModel() {


    val uiState = MutableStateFlow<Resource<Int>>(
        Resource.loading(null))

    fun createTask(title: String, desc: String, catalogID: Long, catalogName: String){

        uiScope.launch {
            var result: Long? = withContext(Dispatchers.IO){
                taskRepository.getMaxTaskID()
            }
            delay(100)
            Log.d("maxID", result.toString())
            var dueDate = ""
            var favoriteStatus = false
            if(catalogName == "Мой день"){
                val sdf = SimpleDateFormat("d/M/yyyy")
                dueDate = sdf.format(Date())
            }
            Log.d("checkus", catalogName)
            if(catalogName == "Важно"){
                favoriteStatus = true
            }

            if(result == null){
                result = 0
            }
            var newId = result!! + 1
            val task = TaskModel(
                newId,
                title,
                desc,
                dueDate,
                false,
                favoriteStatus,
                catalogID
            )
            CoroutineScope(
                Dispatchers.IO
            ).launch {
                Log.d("Check", "hey")
                taskRepository.insertTask(task)
                uiState.value = Resource.success(201)
            }
        }
    }
    override fun handleError(e: Throwable) {

    }
}