package com.example.todoapp.tasks

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.domain.model.CatalogModel
import com.example.domain.model.TaskModel
import com.example.kinopoisk.base.BaseFragment
import com.example.todoapp.R
import com.example.todoapp.tasks.create.TaskCreateFragment
import com.example.todoapp.utils.Status
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject


@Suppress("DEPRECATION", "DEPRECATION")
class TaskListFragment : BaseFragment() {

    private lateinit var navController: NavController
    private val viewModel: TaskListViewModel by inject()
    private lateinit var rvTaskList: RecyclerView
    private lateinit var rvDoneList: RecyclerView
    private lateinit var listName: EditText
    private lateinit var doneTitle: TextView

    private lateinit var addTask: FloatingActionButton
    private var catalogID: Long? = 0
    private var catalogName: String = ""
    private var doneTitleStatus: Boolean = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_task_list, container, false)
    }


    @SuppressLint("UseCompatLoadingForDrawables")
    @InternalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViews(view)
        listName.setText(catalogName)
        viewModel.loadTaskList(catalogID!!, catalogName)
        setAdapter()
        mIth.attachToRecyclerView(rvTaskList)
        setData()
        setHasOptionsMenu(true)



        var toolbar = view.findViewById(R.id.list_toolbar) as Toolbar?

        toolbar?.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24)

        toolbar?.title = "Списки"

        if(catalogName == "Мой день" || catalogName == "Важно" || catalogName == "Задачи"){
            listName.isEnabled = false
        }
        else if(catalogName == ""){
            listName.requestFocus()
        }

        listName.setOnClickListener{
            listName.isCursorVisible = true
        }

        listName.setOnEditorActionListener { v, actionId, event ->
            if(actionId == EditorInfo.IME_ACTION_DONE){
                if(listName.text.toString() == ""){
                    listName.setText("Список без названия")
                }
                listName.isCursorVisible = false
                catalogName = listName.text.toString()
                var catalog = CatalogModel(
                        catalogID!!,
                        catalogName
                )
                viewModel.updateCatalog(catalog)
            }
            return@setOnEditorActionListener false
        }

        toolbar?.setNavigationOnClickListener {
            if(listName.text.toString() == ""){
                catalogName = "Список без названия"
            }
            else{
                catalogName = listName.text.toString()
            }
            var catalog = CatalogModel(
                    catalogID!!,
                    catalogName
            )
            viewModel.updateCatalog(catalog)
            navController.navigate(
                    R.id.action_TaskList_to_Home
            )
        }


        addTask.setOnClickListener {

            val bundle = Bundle()
            bundle.putLong("id", catalogID!!)
            bundle.putString("catalogName", catalogName)

            val taskCreateDialogFragment = TaskCreateFragment(onDismissListener).newInstance()

            if (taskCreateDialogFragment != null) {
                taskCreateDialogFragment.arguments = bundle
                taskCreateDialogFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyle)
                activity?.supportFragmentManager?.let { taskCreateDialogFragment.show(it, "TaskCreate") }
            }

        }

        doneTitle.setOnClickListener {
            if(doneTitleStatus){
                doneTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_chevron_right_24, 0, 0, 0);
                rvDoneList.visibility = View.INVISIBLE
                doneTitleStatus = false
            }
            else{
                doneTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_down_24, 0, 0, 0);
                rvDoneList.visibility = View.VISIBLE
                doneTitleStatus = true
            }

        }
    }


    @InternalCoroutinesApi
    var mIth = ItemTouchHelper(
            object : ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP or ItemTouchHelper.DOWN,
                    ItemTouchHelper.LEFT) {
                override fun onMove(recyclerView: RecyclerView,
                                    viewHolder: ViewHolder, target: ViewHolder): Boolean {
                    val fromPos = viewHolder.adapterPosition
                    val toPos = target.adapterPosition

                    taskDoAdapter.notifyItemMoved(fromPos, toPos)
                    // move item in `fromPos` to `toPos` in adapter.
                    return true // true if moved, false otherwise
                }

                override fun onSwiped(viewHolder: ViewHolder, direction: Int) {
                    //Toast.makeText(context, viewHolder.adapterPosition.toString(), Toast.LENGTH_SHORT).show()
//                    Log.d("what", viewHolder.adapterPosition.toString())
//                    val task: TaskModel = taskDoneAdapter.getItem(viewHolder.adapterPosition)

//                    viewModel.deleteTask(task)
//                    setData()
                }
            })


    @InternalCoroutinesApi
    private val onClickListener = object:
        TaskAdapter.ItemClickListener {
        override fun onItemClick(item: TaskModel) {
            val bundle = Bundle()
            bundle.putLong("id", item.id)
            bundle.putString("catalogName", catalogName)
            Log.d("isIdFrom", item.id.toString())
            navController.navigate(
                R.id.action_TaskList_to_TaskDetail,
                bundle
            )
        }

        override fun updateToDone(task: TaskModel) {
            var updTask = task
            updTask.done = !updTask.done
            viewModel.updateTask(updTask)
            setData()
        }

        override fun updateToFav(task: TaskModel) {
            var updTask = task
            updTask.favorite = !updTask.favorite
            viewModel.updateTask(updTask)
            setData()
        }

        override fun removeTask(id: Long) {
            viewModel.removeTask(id)
            setData()
        }
    }

    private val onDismissListener = object:
            TaskCreateFragment.DismissListener {
        override fun onDismissListener() {
            viewModel.loadTaskList(catalogID!!, catalogName)
        }
    }
    @InternalCoroutinesApi
    private val taskDoneAdapter by lazy {
        TaskAdapter(
            context = context,
            itemClickListener = onClickListener
        )
    }

    @InternalCoroutinesApi
    private val taskDoAdapter by lazy {
        TaskAdapter(
                context = context,
                itemClickListener = onClickListener
        )
    }

    override fun bindViews(view: View) = with(view) {
        catalogID = arguments?.getLong("id")
        catalogName = arguments?.getString("catalogName").toString()
        navController = Navigation.findNavController(this)
        rvTaskList = findViewById(R.id.rvTasklist)
        rvDoneList = findViewById(R.id.rvDoneList)
        addTask = findViewById(R.id.addTask)
        listName = findViewById(R.id.listName)
        doneTitle = findViewById(R.id.doneTitle)
        rvTaskList.layoutManager = LinearLayoutManager(context)
        rvDoneList.layoutManager = LinearLayoutManager(context)
    }

    @InternalCoroutinesApi
    override fun setData() {
        viewModel.loadTaskList(catalogID!!, catalogName)
        lifecycleScope.launch{
            val value = viewModel.uiState
            value.collect {
                when(it.status){
                    Status.LOADING -> {
                        Log.d("status", "loading")
                    }
                    Status.SUCCESS ->{
                        it.data?.let { it ->
                            var list1 = it.filter{ !it.done }
                            var list2 = it.filter{ it.done }
                            if(catalogName == "Важно"){
                                taskDoAdapter.setTaskList(list1!!)
                            }
                            else{
                                taskDoAdapter.setTaskList(list1!!)
                                taskDoneAdapter.setTaskList(list2!!)
                            }
                            if(list2.isEmpty()){
                                doneTitle.visibility = View.INVISIBLE
                            }
                            else{
                                doneTitle.visibility = View.VISIBLE
                            }
                            if(catalogName == "Важно"){
                                doneTitle.visibility = View.INVISIBLE
                            }
                            Log.d("status", it.size.toString())
                        }
                    }
                }
            }
        }
    }

    @InternalCoroutinesApi
    private fun setAdapter(){
        rvTaskList.adapter = taskDoAdapter
        rvDoneList.adapter = taskDoneAdapter
    }




}