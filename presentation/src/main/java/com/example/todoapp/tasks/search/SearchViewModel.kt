package com.example.todoapp.tasks.search

import com.example.domain.model.TaskModel
import com.example.domain.repository.TaskRepository
import com.example.kinopoisk.base.BaseViewModel
import com.example.todoapp.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SearchViewModel(private val taskRepository: TaskRepository): BaseViewModel(){

    val uiState = MutableStateFlow<Resource<List<TaskModel>>>(
        Resource.loading(null))

    fun loadTaskList(){
        uiScope.launch {

            uiState.value = Resource.loading(null)
            val result = withContext(Dispatchers.IO) {
                taskRepository.getAllTasks()
            }
            uiState.value = Resource.success(result)
        }
    }

    override fun handleError(e: Throwable) {
    }
}