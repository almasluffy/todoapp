package com.example.todoapp.tasks

import android.util.Log
import com.example.domain.model.CatalogModel
import com.example.domain.model.TaskModel
import com.example.domain.repository.TaskRepository
import com.example.kinopoisk.base.BaseViewModel
import com.example.todoapp.utils.Resource
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import java.text.SimpleDateFormat
import java.util.*

@Suppress("IMPLICIT_CAST_TO_ANY")
class TaskListViewModel(private val taskRepository: TaskRepository): BaseViewModel() {

    private var catalogID: Long = 0
    private var catalogName: String = ""

     val uiState = MutableStateFlow<Resource<List<TaskModel>>>(
         Resource.loading(null))

    fun loadTaskList(catalogID: Long, catalogName: String){
        this.catalogID = catalogID
        this.catalogName = catalogName

        uiScope.launch  {
            uiState.value = Resource.loading(null)

            val result = withContext(Dispatchers.IO) {
                taskRepository.getTasks(catalogID)
            }
            uiState.value = Resource.success(result)

            if(catalogName == "Мой день"){
                val sdf = SimpleDateFormat("d/M/yyyy")
                var dueDate = sdf.format(Date())
                Log.d("today1", dueDate)
                Log.d("here today", "hey")
                val result = withContext(Dispatchers.IO) {
                    taskRepository.getTodayList(dueDate)
                }
                uiState.value = Resource.success(result)
            }

            if(catalogName == "Важно"){
                val result = withContext(Dispatchers.IO) {
                    taskRepository.getFavoriteList()
                }
                uiState.value = Resource.success(result)
            }

            else if(catalogName == "Задачи"){
                Log.d("here zadachi", "hey")
                val result = withContext(Dispatchers.IO) {
                    taskRepository.getTasks(catalogID)
                }
                uiState.value = Resource.success(result)
            }

            else if(catalogID > 0){
                Log.d("here zadachi", "hey")
                val result = withContext(Dispatchers.IO) {
                    taskRepository.getTasks(catalogID)
                }
                uiState.value = Resource.success(result)
            }
        }
    }

    fun loadAllTaskList(){
        uiScope.launch {
            uiState.value = Resource.loading(null)
            val result = withContext(Dispatchers.IO) {
                taskRepository.getAllTasks()
            }
            uiState.value = Resource.success(result)
        }
    }

     fun updateTask(task: TaskModel){
         CoroutineScope(
                 Dispatchers.IO).launch {
             Log.d("Check", "hey")
             taskRepository.updateTask(task)
         }
    }

    fun removeTask(id: Long){
        uiScope.launch {
            taskRepository.deleteTask(id)
        }
    }

    fun updateCatalog(catalog: CatalogModel){
        uiScope.launch {
            taskRepository.updateCatalog(catalog)
        }
    }

    override fun handleError(e: Throwable) {
    }

}