package com.example.todoapp.tasks.detail

import android.util.Log
import com.example.domain.model.TaskModel
import com.example.domain.repository.TaskRepository
import com.example.kinopoisk.base.BaseViewModel
import com.example.todoapp.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TaskDetailViewModel(private val taskRepository: TaskRepository): BaseViewModel() {


    val uiState = MutableStateFlow<Resource<TaskModel>>(
        Resource.loading(null))

    fun getTask(id: Long){
        uiScope.launch  {
            val result = withContext(Dispatchers.IO) {
                taskRepository.getTask(id)
            }
            Log.d("whatID_2", result.id.toString())
            uiState.value = Resource.success(result)
        }
    }

    fun updateTask(task: TaskModel){
        uiScope.launch  {
            taskRepository.updateTask(task)
        }
    }

    fun deleteTask(id: Long){
        uiScope.launch  {
            taskRepository.deleteTask(id)
            uiState.value = Resource.deleteStatus(null)
        }
    }

    override fun handleError(e: Throwable) {

    }
}