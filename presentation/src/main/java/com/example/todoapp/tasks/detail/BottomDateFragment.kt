package com.example.todoapp.tasks.detail

import android.app.DatePickerDialog
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.DatePicker
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.example.todoapp.R
import com.example.todoapp.tasks.create.TaskCreateFragment
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

class BottomDateFragment(private val onDismissListener: DateDismissListener):
        BottomSheetDialogFragment(), DatePickerDialog.OnDateSetListener {

    private lateinit var doneText: TextView
    private lateinit var todayView: TextView
    private lateinit var tomorrowView: TextView
    private lateinit var pickDate: TextView

    var day = 0
    var month = 0
    var year = 0

    var savedDay = 0
    var savedMonth = 0
    var savedYear = 0

    fun newInstance(): BottomDateFragment? {
        return BottomDateFragment(onDismissListener)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bottom_date, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViews(view)

        doneText.setOnClickListener {
            dismiss()
        }

        val formatter = DateTimeFormatter.ofPattern( "d/M/yyyy")

        todayView.setOnClickListener {
            val today = LocalDate.now()
            val todayParsed  = today.format(formatter)
            onDismissListener.onDismissListener(todayParsed)
            dismiss()
        }

        tomorrowView.setOnClickListener {
            val tomorrow = LocalDate.now().plusDays(1)
            val tomorrowParsed = tomorrow.format(formatter)
            onDismissListener.onDismissListener(tomorrowParsed)
            dismiss()
        }

        pickDate.setOnClickListener {
            getDateCalendar()
            DatePickerDialog(requireContext(), this, year, month, day).show()
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun bindViews(view: View) = with(view) {
        doneText = findViewById(R.id.doneBottomView)
        todayView = findViewById(R.id.todayDateView)
        tomorrowView = findViewById(R.id.tomorrowDateView)
        pickDate = findViewById(R.id.pickDateViewBottom)
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        savedDay = dayOfMonth
        savedMonth = month+1
        savedYear = year

        val savedDate = "$savedDay/$savedMonth/$savedYear"
        onDismissListener.onDismissListener(savedDate)
        dismiss()
    }

    private fun getDateCalendar(){
        val cal: Calendar = Calendar.getInstance()
        day = cal.get(Calendar.DAY_OF_MONTH)
        month = cal.get(Calendar.MONTH)
        year = cal.get(Calendar.YEAR)
    }

    interface DateDismissListener{
        fun onDismissListener(dueDate: String)
    }

}