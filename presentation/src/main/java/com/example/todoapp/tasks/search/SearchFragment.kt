package com.example.todoapp.tasks.search

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.domain.model.TaskModel
import com.example.todoapp.R
import com.example.todoapp.tasks.TaskAdapter
import com.example.todoapp.tasks.TaskListViewModel
import com.example.todoapp.utils.Status
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class SearchFragment : Fragment() {

    private lateinit var navController: NavController
    private val viewModel: TaskListViewModel by inject()
    private lateinit var rvSearchList: RecyclerView
    private lateinit var searchView: SearchView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindViews(view)

        val searchIcon = searchView.findViewById<ImageView>(R.id.search_mag_icon)
        searchIcon.setColorFilter(Color.WHITE)

        val textView = searchView.findViewById<TextView>(R.id.search_src_text)
        textView.setTextColor(Color.WHITE)

        val cancelIcon = searchView.findViewById<ImageView>(R.id.search_close_btn)
        cancelIcon.setColorFilter(Color.WHITE)



        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                taskAdapter.filter.filter(newText)
                return false
            }

        })

        viewModel.loadAllTaskList()
        setAdapter()
        setData()
    }

    private val taskAdapter by lazy {
        TaskAdapter(
                context = context,
                itemClickListener = onClickListener
        )
    }

    private val onClickListener = object:
            TaskAdapter.ItemClickListener {
        override fun onItemClick(item: TaskModel) {
            val bundle = Bundle()
            bundle.putLong("id", item.id)
            bundle.putString("catalogName", "search")
            navController.navigate(
                    R.id.action_TaskList_to_TaskDetail,
                    bundle
            )
        }

        override fun updateToDone(task: TaskModel) {
            var updTask = task
            updTask.done = !updTask.done
            viewModel.updateTask(updTask)
            setData()
        }

        override fun updateToFav(task: TaskModel) {
            var updTask = task
            updTask.favorite = !updTask.favorite
            viewModel.updateTask(updTask)
            setData()
        }

        override fun removeTask(id: Long) {
            viewModel.removeTask(id)
            setData()
        }
    }

     fun bindViews(view: View) = with(view) {
        navController = Navigation.findNavController(this)
        rvSearchList = findViewById(R.id.rvSearchList)
         searchView = findViewById(R.id.task_search)
        rvSearchList.layoutManager = LinearLayoutManager(context)
    }

    fun setData() {
        viewModel.loadAllTaskList()
        lifecycleScope.launch{
            val value = viewModel.uiState
            value.collect {
                when(it.status){
                    Status.LOADING -> {
                        Log.d("status", "loading")
                    }
                    Status.SUCCESS ->{
                        it.data?.let { it ->
                            taskAdapter.setTaskList(it)
                        }
                    }
                }
            }
        }
    }

    private fun setAdapter(){
        rvSearchList.adapter = taskAdapter
    }
}