package com.example.todoapp.tasks.create

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.core.content.ContextCompat.getSystemService
import androidx.lifecycle.lifecycleScope
import com.example.todoapp.R
import com.example.todoapp.utils.Status
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject


class TaskCreateFragment(private val onDismissListener: DismissListener)
    : BottomSheetDialogFragment() {

    private val viewModel: TaskCreateViewModel by inject()

    private lateinit var title: EditText

    private var catalogID: Long? = 0
    private var catalogName: String = ""

    fun newInstance(): TaskCreateFragment? {
        return TaskCreateFragment(onDismissListener)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        catalogID = arguments?.getLong("id")
        catalogName = arguments?.getString("catalogName").toString()
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_task_create, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViews(view)
        setData()
    }

    private fun bindViews(view: View) = with(view) {

        title = findViewById(R.id.titleTaskCreate)
        title.requestFocus()
        title.setOnEditorActionListener { v, actionId, event ->
            if(actionId == EditorInfo.IME_ACTION_DONE){
                if(title.text.toString() == ""){
                    dismiss()
                }
                else {
                    viewModel.createTask(title.text.toString(), "", catalogID!!, catalogName)
                }
            }
            return@setOnEditorActionListener false
        }
    }

    private fun setData(){
        lifecycleScope.launch{
            val value = viewModel.uiState
            value.collect {
                when(it.status){
                    Status.SUCCESS ->{
                        onDismissListener.onDismissListener()
                        dismiss()
                    }
                }
            }
        }
    }

    interface DismissListener{
        fun onDismissListener()
    }

}