package com.example.todoapp.tasks.detail

import android.annotation.SuppressLint
import android.graphics.Color.blue
import android.graphics.Paint
import android.media.Image
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.domain.model.TaskModel
import com.example.kinopoisk.base.BaseFragment
import com.example.todoapp.R
import com.example.todoapp.tasks.create.TaskCreateFragment
import com.example.todoapp.utils.Status
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import java.text.SimpleDateFormat
import java.text.DateFormat
import java.text.DateFormatSymbols
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

class TaskDetailFragment: BaseFragment(){

    private val viewModel: TaskDetailViewModel by inject()

    private lateinit var navController: NavController

    private lateinit var title: EditText
    private lateinit var pickDate: LinearLayout
    private lateinit var dateText: TextView
    private lateinit var description: EditText
    private lateinit var doneCircle: ImageView
    private lateinit var favBtn: ImageView
    private lateinit var cancelDateImg: ImageView
    private lateinit var deleteBtn: ImageView

    private var taskID: Long? = 0
    private var task: TaskModel? = null
    private var catalogName: String = ""


    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_task_detail, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViews(view)
        setData()

        var toolbar = view.findViewById(R.id.detail_toolbar) as Toolbar?

        toolbar?.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24)

        if(catalogName == "Поиск"){
            toolbar?.title = "Поиск"
        }
        else{
            toolbar?.title = "Задачи"
        }

        toolbar?.setNavigationOnClickListener {
            if(catalogName == "Поиск"){
                navController.navigate(
                    R.id.action_TaskDetail_to_Search
                )
            }
            else{
                val bundle = Bundle()
                bundle.putLong("id", task!!.catalogID)
                bundle.putString("catalogName", catalogName)
                navController.navigate(
                    R.id.action_TaskDetail_to_TaskList,
                    bundle
                )
            }
        }

        doneCircle.setOnClickListener {
            task!!.done = !task!!.done
            viewModel.updateTask(task!!)
            setData()
        }

        favBtn.setOnClickListener {
            task!!.favorite = !task!!.favorite
            viewModel.updateTask(task!!)
            setData()
        }

        pickDate.setOnClickListener {

            val dateBottomDialog = BottomDateFragment(onDismissListener).newInstance()

            if (dateBottomDialog != null) {
                dateBottomDialog.setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyle)
                activity?.supportFragmentManager?.let { dateBottomDialog.show(it, "BottomDate") }
            }
        }

        deleteBtn.setOnClickListener {
            task?.id?.let { it1 -> viewModel.deleteTask(it1) }
        }

        title.setOnClickListener{
            title.isCursorVisible = true
        }

        cancelDateImg.setOnClickListener {
            dateText.text = "Добавить дату выполнения"
            dateText.setTextColor(resources.getColor(R.color.white))
            task?.dueDate  = ""
            viewModel.updateTask(task!!)
            cancelDateImg.visibility = View.INVISIBLE
        }

        title.setOnEditorActionListener { v, actionId, event ->
            if(actionId == EditorInfo.IME_ACTION_DONE){
                title.isCursorVisible = false
                task!!.title = title.text.toString()
                viewModel.updateTask(task!!)
                setData()
            }
            return@setOnEditorActionListener false
        }

        description.setOnClickListener{
            description.isCursorVisible = true
        }

        description.setOnEditorActionListener { v, actionId, event ->
            if(actionId == EditorInfo.IME_ACTION_DONE){
                description.isCursorVisible = false
                task!!.description = description.text.toString()
                viewModel.updateTask(task!!)
                setData()
            }
            return@setOnEditorActionListener false
        }

    }


    override fun bindViews(view: View) = with(view){
        navController = Navigation.findNavController(this)
        taskID = arguments?.getLong("id")
        catalogName = arguments?.getString("catalogName").toString()
        dateText = findViewById(R.id.dateText)
        pickDate = findViewById(R.id.pickDateView)
        title = findViewById(R.id.editTitle)
        favBtn = findViewById(R.id.favoriteViewDetail)
        description = findViewById(R.id.editDesc)
        doneCircle = findViewById(R.id.done_circle)
        deleteBtn = findViewById(R.id.deleteTaskBtn)
        cancelDateImg = findViewById(R.id.cancelDate)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private val onDismissListener = object:
            BottomDateFragment.DateDismissListener {
        override fun onDismissListener(dueDate: String) {
            setDate(dueDate)
            cancelDateImg.visibility = View.VISIBLE
            task?.dueDate = dueDate
            viewModel.updateTask(task!!)
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun setData() {

        viewModel.getTask(taskID!!)

        Log.d("isIdTo", taskID.toString())

        lifecycleScope.launch{
            val value = viewModel.uiState
            value.collect {
                when(it.status){
                    Status.SUCCESS ->{
                        it.data.let {
                            if(it !=null){
                                task = it
                                if(it.dueDate == ""){
                                    cancelDateImg.visibility = View.INVISIBLE
                                }
                                else{
                                    setDate(it.dueDate)
                                    dateText.setTextColor(resources.getColor(R.color.blue))
                                }
                                title.setText(it.title)
                                description.setText(it.description)
                                if(task!!.done){
                                    doneCircle.setImageResource(R.drawable.ic_baseline_check_circle_24)
                                    title.paintFlags = title.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                                }
                                else{
                                    doneCircle.setImageResource(R.drawable.circle_border)
                                    title.paintFlags = title.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
                                }
                                if(task!!.favorite){
                                    favBtn.setImageResource(R.drawable.ic_baseline_star_24)
                                }
                                else{
                                    favBtn.setImageResource(R.drawable.ic_baseline_star_outline_24)
                                }
                            }
                        }
                    }
                    Status.DELETE ->{

                        val bundle = Bundle()
                        bundle.putLong("id", task!!.catalogID)
                        navController.navigate(
                                R.id.action_TaskDetail_to_TaskList,
                                bundle
                        )

                    }
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.O)
    fun setDate(dueDate: String){

        val formatter = DateTimeFormatter.ofPattern( "d/M/yyyy")

        val today = LocalDate.now()

        today.dayOfWeek

        val tomorrow = LocalDate.now().plusDays(1)

        val todayParsed  = today.format(formatter)
        val tomorrowParsed = tomorrow.format(formatter)

        if(dueDate == todayParsed){
            dateText.text = "  Срок: сегодня"
        }
        else if(dueDate == tomorrowParsed){
            dateText.text = "  Срок: завтра"
        }
        else {
            val formatter = DateTimeFormatter.ofPattern("d/M/yyyy")
            val dt = LocalDate.parse(dueDate, formatter)
            val date = SimpleDateFormat("d/M/yyyy").parse(dueDate)
            var dSymbol = DateFormatSymbols.getInstance()
            var monthStr = dSymbol.months[dt.monthValue-1]

            val formatted = SimpleDateFormat("d/M/yyyy").parse(dueDate)
            var c = Calendar.getInstance();
            c.time = formatted;
            var dayOfWeek = c.get(Calendar.DAY_OF_MONTH);

            dateText.text = "Cрок: " + dt.dayOfWeek + ", "  + dayOfWeek.toString() + " " + monthStr
        }
        Calendar.DAY_OF_MONTH
        dateText.setTextColor(resources.getColor(R.color.blue))
    }

}