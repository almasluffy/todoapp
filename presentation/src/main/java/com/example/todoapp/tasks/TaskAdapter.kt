package com.example.todoapp.tasks

import android.content.Context
import android.graphics.Paint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.example.domain.model.TaskModel
import com.example.kinopoisk.base.BaseViewHolder
import com.example.todoapp.R
import com.google.android.play.core.tasks.Task
import java.util.*
import kotlin.collections.ArrayList

class TaskAdapter(
    private val context: Context?,
    private val itemClickListener: ItemClickListener
): RecyclerView.Adapter<BaseViewHolder>(), Filterable {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var taskList = emptyList<TaskModel>()
    private var filterList = emptyList<TaskModel>()

    private val TYPE_DO = 1
    private val TYPE_DONE = 2

    inner class TaskViewHolder(itemView: View, type: Int): BaseViewHolder(itemView) {

        private var taskTitle: TextView = itemView.findViewById(R.id.taskTitle)
        private var toDone: ImageView = itemView.findViewById(R.id.toDoneBtn)
        private var addToFav: ImageView = itemView.findViewById(R.id.addToFav)
        private var removeImg: ImageView = itemView.findViewById(R.id.removeTask)

        private var type = type

        fun bind(task: TaskModel){
            if(task.title.length > 15){
                taskTitle.text = task.title.take(12) + "..."
            }
            else if(task.title.length <= 15){
                taskTitle.text = task.title
            }

            if(task.done){
                toDone.setImageResource(R.drawable.ic_baseline_check_circle_24)
            }
            else if(!task.done){
                toDone.setImageResource(R.drawable.circle_border)
            }

            if(type == 2){
                taskTitle.paintFlags = taskTitle.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            }

            Log.d("fav_stat", task.favorite.toString())
            if(task.favorite){
                addToFav.setImageResource(R.drawable.ic_baseline_star_24)
            }
            else{
                addToFav.setImageResource(R.drawable.ic_baseline_star_outline_24)
            }

            toDone.setOnClickListener {
                itemClickListener.updateToDone(task)
            }

            addToFav.setOnClickListener {
                itemClickListener.updateToFav(task)
            }

            removeImg.setOnClickListener {
                itemClickListener.removeTask(task.id)
            }
        }


        fun setItemClick(item: TaskModel) {
            itemView.setOnClickListener{
                itemClickListener.onItemClick(item)
            }
        }

        override fun clear() { }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val itemView = inflater.inflate(R.layout.for_task, parent, false)
        Log.d("viewType", viewType.toString())
        return TaskViewHolder(itemView, viewType)
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if(charSearch.isEmpty()){
                    filterList = taskList
                }
                else{
                    val resultList = ArrayList<TaskModel>()
                    for(task in taskList){
                        if(task.title.toLowerCase(Locale.ROOT).contains(charSearch.toLowerCase(Locale.ROOT))){
                            resultList.add(task)
                        }
                        filterList = resultList
                    }
                }
                val filterResults = FilterResults()
                filterResults.values = filterList
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                filterList = results?.values as ArrayList<TaskModel>
                notifyDataSetChanged()
            }

        }
    }

    override fun getItemViewType(position: Int): Int {
        if(taskList.get(position).done){
            return TYPE_DONE
        }
        else{
            return TYPE_DO
        }
    }
    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        if(holder is TaskViewHolder){
            val task = taskList[position]
            holder.bind(task)
            holder.setItemClick(task)
        }
    }

    internal fun setTaskList(taskList: List<TaskModel>) {
        this.taskList = taskList
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int =
        taskList.size

    fun getItem(position: Int): TaskModel{
        return taskList[position]
    }


    interface ItemClickListener {
        fun onItemClick(item: TaskModel)
        fun updateToDone(task: TaskModel)
        fun updateToFav(task: TaskModel)
        fun removeTask(id: Long)
    }

}