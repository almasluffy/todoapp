package com.example.todoapp.catalog.create

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.todoapp.R
import com.example.todoapp.tasks.create.TaskCreateViewModel
import com.example.todoapp.utils.Status
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class CatalogCreateFragment : Fragment() {

    private lateinit var navController: NavController
    private val viewModel: CatalogCreateViewModel by inject()

    private lateinit var name: EditText
    private lateinit var createBtn: Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_catalog_create, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViews(view)
        setData()
    }

    private fun bindViews(view: View) = with(view) {
        navController = Navigation.findNavController(this)
        name = findViewById(R.id.nameEdit)
        createBtn = findViewById(R.id.createCatalog)

        createBtn.setOnClickListener {
            viewModel.createCatalog(name.text.toString())
        }
    }

    private fun setData(){
        lifecycleScope.launch{
            val value = viewModel.uiState
            value.collect {
                when(it.status){
                    Status.SUCCESS ->{
                        navController.navigate(
                            R.id.action_CatalogCreate_to_MainFragment
                        )
                    }
                }
            }
        }
    }
}