package com.example.todoapp.catalog.list

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.domain.model.CatalogModel
import com.example.domain.model.TaskModel
import com.example.kinopoisk.base.BaseViewHolder
import com.example.todoapp.R

class CatalogAdapter(
    private val context: Context?,
    private val itemClickListener: ItemClickListener
): RecyclerView.Adapter<CatalogAdapter.CatalogViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var catalogList = emptyList<CatalogModel>()


    inner class CatalogViewHolder(itemView: View): BaseViewHolder(itemView) {
        private val catalogName: TextView = itemView.findViewById(R.id.catalogName)
        private val deleteCatalog: ImageView = itemView.findViewById(R.id.deleteCatalog)

        @SuppressLint("ResourceAsColor", "ClickableViewAccessibility")
        fun bind(catalog: CatalogModel){
            catalogName.text = catalog.name

            deleteCatalog.setOnClickListener {
                itemClickListener.deleteCatalog(catalog)
            }
            itemView.setOnTouchListener { v, event ->
                when (event?.action) {
                    MotionEvent.ACTION_DOWN -> {
                        itemView.setBackgroundColor(view.resources.getColor(R.color.darkGrey))
                    }
                }
                v?.onTouchEvent(event) ?: true
            }
        }


        fun setItemClick(item: CatalogModel) {
            itemView.setOnClickListener{
                itemClickListener.onItemClick(item)
            }

        }

        override fun clear() { }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CatalogViewHolder {
        val itemView = inflater.inflate(R.layout.for_catalog, parent, false)
        return CatalogViewHolder(itemView)
    }
    override fun onBindViewHolder(holder: CatalogViewHolder, position: Int) {
        val catalog = catalogList[position]
        holder.bind(catalog)
        holder.setItemClick(catalog)
    }

    internal fun setCatalogList(catalogList: List<CatalogModel>) {
        this.catalogList = catalogList
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int =
        catalogList.size

    interface ItemClickListener {
        fun onItemClick(item: CatalogModel)
        fun deleteCatalog(catalog: CatalogModel)
    }
}