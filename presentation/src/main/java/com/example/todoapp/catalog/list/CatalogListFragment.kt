package com.example.todoapp.catalog.list

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.domain.model.CatalogModel
import com.example.kinopoisk.base.BaseFragment
import com.example.todoapp.R
import com.example.todoapp.utils.Status
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class CatalogListFragment : BaseFragment() {

    private lateinit var navController: NavController
    private val viewModel: CatalogListViewModel by inject()
    private lateinit var rvCatalogList: RecyclerView

    private lateinit var addCatalog: FloatingActionButton

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_catalog_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.loadCatalogList()
        bindViews(view)
        setAdapter()
        setData()

        addCatalog.setOnClickListener {
            navController.navigate(
                R.id.action_CatalogList_to_CatalogCreate
            )
        }
    }


    private val onClickListener = object:
        CatalogAdapter.ItemClickListener {
        override fun onItemClick(item: CatalogModel) {
            val bundle = Bundle()
            bundle.putLong("id", item.id)
            bundle.putString("listName", item.name)
            navController.navigate(
                R.id.action_CatalogList_to_TaskList,
                bundle
            )
        }

        override fun deleteCatalog(catalog: CatalogModel) {
        }
    }
    private val catalogAdapter by lazy {
        CatalogAdapter(
            context = context,
            itemClickListener = onClickListener
        )
    }

    override fun bindViews(view: View) = with(view) {
        navController = Navigation.findNavController(this)
        rvCatalogList = findViewById(R.id.rvCatalogList)
        addCatalog = findViewById(R.id.addCatalog)
        rvCatalogList.layoutManager = LinearLayoutManager(context)
    }

    override fun setData() {
        lifecycleScope.launch{
            val value = viewModel.uiState
            value.collect {
                when(it.status){
                    Status.LOADING -> {
                        Log.d("status", "loading")
                    }
                    Status.SUCCESS ->{
                        it.data.let {
                            catalogAdapter.setCatalogList(it!!)
                            Log.d("status", it.size.toString())
                        }
                    }
                }
            }
        }
    }

    private fun setAdapter(){
        rvCatalogList.adapter = catalogAdapter
    }

}