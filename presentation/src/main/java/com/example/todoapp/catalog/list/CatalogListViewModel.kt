package com.example.todoapp.catalog.list

import android.util.Log
import com.example.domain.model.CatalogModel
import com.example.domain.repository.CatalogRepository
import com.example.kinopoisk.base.BaseViewModel
import com.example.todoapp.utils.Resource
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow

class CatalogListViewModel(private val catalogRepository: CatalogRepository): BaseViewModel() {

    val uiState = MutableStateFlow<Resource<List<CatalogModel>>>(
        Resource.loading(null))


    fun loadCatalogList(){
        uiScope.launch  {
            uiState.value = Resource.loading(null)
            val result = withContext(Dispatchers.IO) {
                catalogRepository.getCatalogList()
            }
            Log.d("viewRes", result.size.toString())
            uiState.value = Resource.success(result)
        }

    }

    fun createCatalog(){
        uiScope.launch {

            var result: Long? = withContext(Dispatchers.IO) {
                catalogRepository.getMaxCatalogID()
            }
            if (result == null) {
                result = 0
            }
            delay(100)
            // Log.d("maxID", result.toString())
            var newId = result!! + 1
            val catalog = CatalogModel(
                    newId,
                    ""
            )
            var result2 = withContext(Dispatchers.IO) {
                catalogRepository.insertCatalog(catalog)
            }
            uiState.value = Resource.updateStatus(null, newId.toString())
        }
    }

    fun deleteCatalog(catalog: CatalogModel){uiScope.launch  {
        val result = withContext(Dispatchers.IO) {
            catalogRepository.deleteCatalog(catalog)
        }
        CoroutineScope(
                Dispatchers.IO
        ).launch {
            Log.d("Check", "hey")
            catalogRepository.deleteAllCatalogTasks(catalog.id)
        }
        uiState.value = Resource.deleteStatus(null)
    }
    }


    override fun handleError(e: Throwable) {
    }

}