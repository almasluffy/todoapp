package com.example.todoapp.catalog.create

import android.util.Log
import com.example.domain.model.CatalogModel
import com.example.domain.model.TaskModel
import com.example.domain.repository.CatalogRepository
import com.example.kinopoisk.base.BaseViewModel
import com.example.todoapp.utils.Resource
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow

class CatalogCreateViewModel(private val catalogRepository: CatalogRepository): BaseViewModel() {

    val uiState = MutableStateFlow<Resource<Int>>(
        Resource.loading(null))

    fun createCatalog(name: String){

        uiScope.launch {
            var result: Long? = withContext(Dispatchers.IO){
                catalogRepository.getMaxCatalogID()
            }
            if(result == null){
                result = 0
            }
            delay(100)
            Log.d("maxID", result.toString())
            var newId = result!! + 1
            val catalog = CatalogModel(
                newId,
                name
            )
            CoroutineScope(
                Dispatchers.IO
            ).launch {
                Log.d("Check", "hey")
                catalogRepository.insertCatalog(catalog)
                uiState.value = Resource.success(201)
            }
        }
    }
    override fun handleError(e: Throwable) {

    }
}