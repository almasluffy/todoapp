package com.example.domain.model

data class CatalogModel(
    val id: Long,
    val name: String
)