package com.example.domain.model

import java.time.LocalDateTime
import java.util.*

data class TaskModel(
        val id: Long,
        var title: String,
        var description: String,
        var dueDate: String,
        var done: Boolean,
        var favorite: Boolean,
        val catalogID: Long
)