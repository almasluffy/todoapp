package com.example.domain.repository

import com.example.domain.model.CatalogModel
import com.example.domain.model.TaskModel
import kotlinx.coroutines.flow.StateFlow

interface TaskRepository {

    suspend fun getAllTasks(): List<TaskModel>
    suspend fun getTasks(catalogID: Long): List<TaskModel>
    suspend fun getTask(id: Long) : TaskModel
    suspend fun getMaxTaskID(): Long?
    suspend fun updateTask(task: TaskModel)
    suspend fun insertTask(task: TaskModel)

    suspend fun deleteTask(id: Long)

    suspend fun getTodayList(dueDate: String): List<TaskModel>
    suspend fun getFavoriteList(): List<TaskModel>

    suspend fun createCatalog(catalog: CatalogModel)
    suspend fun getMaxCatalogId(): Long

    suspend fun updateCatalog(catalog: CatalogModel)
}