package com.example.domain.repository

import com.example.domain.model.CatalogModel
import com.example.domain.model.TaskModel

interface CatalogRepository {
    suspend fun getCatalogList(): List<CatalogModel>
    suspend fun getCatalog(id: Long) : CatalogModel
    suspend fun getMaxCatalogID(): Long?
    suspend fun insertCatalog(catalog: CatalogModel)
    suspend fun deleteCatalog(catalog: CatalogModel)
    suspend fun deleteAllCatalogTasks(id: Long)
}