package com.example.data.repository

import android.util.Log
import com.example.data.ToDoDatabase.dao.CatalogDao
import com.example.data.ToDoDatabase.dao.TaskDao
import com.example.data.map.CatalogMapper
import com.example.data.map.TaskMapper
import com.example.domain.model.CatalogModel
import com.example.domain.model.TaskModel
import com.example.domain.repository.TaskRepository
import java.lang.Exception

class TaskRepositoryImpl(private val taskDao: TaskDao, private val catalogDao: CatalogDao): TaskRepository {

    private val taskMapper = TaskMapper()
    private val catalogMapper = CatalogMapper()

    override suspend fun getTasks(catalogID: Long): List<TaskModel> {
        val list = taskDao.getTaskList(catalogID)
        return list.map { list -> taskMapper.mapFromEntity(list) }
    }

    override suspend fun getAllTasks(): List<TaskModel> {
        val list = taskDao.getAllTasks()
        return list.map { list -> taskMapper.mapFromEntity(list) }
    }

    override suspend fun getTask(id: Long): TaskModel {
        var task = taskDao.getTask(id)
        return taskMapper.mapFromEntity(taskDao.getTask(id))
    }

    override suspend fun updateTask(task: TaskModel) {
        taskDao.update(taskMapper.mapToEntity(task))
    }

    override suspend fun insertTask(task: TaskModel) {
        taskDao.insert(taskMapper.mapToEntity(task))
    }

    override suspend fun deleteTask(id: Long) {
        taskDao.deleteById(id)
    }

    override suspend fun getMaxTaskID(): Long? {
       return taskDao.getMaxTaskID()
    }

    override suspend fun getTodayList(dueDate: String): List<TaskModel> {
        val list = taskDao.getTodayTaskList(dueDate)
        return list.map { list -> taskMapper.mapFromEntity(list) }
    }

    override suspend fun getFavoriteList(): List<TaskModel> {
        val list = taskDao.getFavoriteTaskList()
        return list.map { list -> taskMapper.mapFromEntity(list) }
    }

    override suspend fun createCatalog(catalog: CatalogModel) {
        catalogDao.insert(catalogMapper.mapToEntity(catalog))
    }

    override suspend fun getMaxCatalogId(): Long {
        return catalogDao.getMaxCatalogID()
    }

    override suspend fun updateCatalog(catalog: CatalogModel) {
        catalogDao.update(catalogMapper.mapToEntity(catalog))
    }
}

