package com.example.data.repository

import com.example.data.ToDoDatabase.dao.CatalogDao
import com.example.data.ToDoDatabase.dao.TaskDao
import com.example.data.map.CatalogMapper
import com.example.data.map.TaskMapper
import com.example.domain.model.CatalogModel
import com.example.domain.model.TaskModel
import com.example.domain.repository.CatalogRepository

class CatalogRepositoryImpl(private val catalogDao: CatalogDao, private val taskDao: TaskDao): CatalogRepository{

    private val catalogMapper = CatalogMapper()

    override suspend fun getCatalogList(): List<CatalogModel> {
        val list = catalogDao.getCatalogList()
        return list.map { list -> catalogMapper.mapFromEntity(list) }
    }

    override suspend fun getCatalog(id: Long): CatalogModel {
        return catalogMapper.mapFromEntity(catalogDao.getCatalog(id))
    }

    override suspend fun insertCatalog(catalog: CatalogModel) {
        catalogDao.insert(catalogMapper.mapToEntity(catalog))
    }

    override suspend fun getMaxCatalogID(): Long? {
        return catalogDao.getMaxCatalogID()
    }

    override suspend fun deleteCatalog(catalog: CatalogModel) {
        catalogDao.delete(catalogMapper.mapToEntity(catalog))
    }

    override suspend fun deleteAllCatalogTasks(id: Long) {
        taskDao.deleteAllCatalogTasks(id)
    }
}