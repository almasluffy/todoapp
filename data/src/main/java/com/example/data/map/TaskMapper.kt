package com.example.data.map

import com.example.data.ToDoDatabase.entity.TaskEntity
import com.example.domain.model.TaskModel
import org.w3c.dom.Entity

class TaskMapper: Mapper<TaskEntity, TaskModel> {

    override fun mapToEntity(type: TaskModel): TaskEntity {
        var done = 0
        var favorite = 0

        if(type.done){
            done = 1
        }
        if(type.favorite){
            favorite = 1
        }
        return TaskEntity(
            type.id, type.title,
            type.description,
            type.dueDate,
            done,
            favorite,
            type.catalogID)
    }

    override fun mapFromEntity(type: TaskEntity): TaskModel {
        var done = false
        var favorite = false

        if(type.done == 1){
            done = true
        }
        if(type.favorite == 1){
            favorite = true
        }
        return TaskModel(
            type.id, type.title,
            type.description,
            type.dueDate,
            done,
            favorite,
            type.catalogID
        )
    }

}