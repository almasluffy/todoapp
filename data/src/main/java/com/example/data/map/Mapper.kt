package com.example.data.map

interface Mapper<E,D> {
    fun mapFromEntity(type: E): D

    fun mapToEntity(type: D): E
}