package com.example.data.map

import com.example.data.ToDoDatabase.entity.CatalogEntity
import com.example.domain.model.CatalogModel

class CatalogMapper: Mapper<CatalogEntity, CatalogModel> {

    override fun mapToEntity(type: CatalogModel): CatalogEntity {
        return CatalogEntity(
            type.id,
            type.name
        )
    }

    override fun mapFromEntity(type: CatalogEntity): CatalogModel {
        return CatalogModel(
            type.id,
            type.name
        )
    }

}