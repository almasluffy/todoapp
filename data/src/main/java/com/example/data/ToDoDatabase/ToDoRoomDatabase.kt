package com.example.data.ToDoDatabase

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.data.ToDoDatabase.dao.CatalogDao
import com.example.data.ToDoDatabase.dao.TaskDao
import com.example.data.ToDoDatabase.entity.CatalogEntity
import com.example.data.ToDoDatabase.entity.TaskEntity
import com.example.domain.model.TaskModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.*

@Database(entities = arrayOf(TaskEntity::class, CatalogEntity::class), version = 2, exportSchema = false)
abstract class ToDoRoomDatabase: RoomDatabase() {

    abstract fun taskDao(): TaskDao
    abstract fun catalogDao(): CatalogDao

    companion object {
        @Volatile
        private var INSTANCE: ToDoRoomDatabase? = null

        fun getDatabase(
            context: Context,
            scope: CoroutineScope
        ): ToDoRoomDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    ToDoRoomDatabase::class.java,
                    "todo_database"
                )
                    .fallbackToDestructiveMigration()
                    .build()
                INSTANCE = instance
                return instance
            }
        }

    }
  //  .addCallback(ToDoDatabaseCallback(scope))
//    private class ToDoDatabaseCallback(
//        private val scope: CoroutineScope
//    ): RoomDatabase.Callback() {
//        @RequiresApi(Build.VERSION_CODES.O)
//        override fun onOpen(db: SupportSQLiteDatabase) {
//            super.onOpen(db)
//            INSTANCE?.let { database ->
//                scope.launch {
//                    populateDatabase(database.taskDao(), database.catalogDao())
//                }
//            }
//        }
//
//        @SuppressLint("SimpleDateFormat")
//        suspend fun populateDatabase(taskDao: TaskDao, catalogDao: CatalogDao) {
//            taskDao.deleteAll()
//            catalogDao.deleteAll()
//
//            val sdf = SimpleDateFormat("dd/M/yyyy")
//            var currentDate = sdf.format(Date())
//
//            var task1 = TaskEntity(
//                1,
//                "Buy Tickets",
//                "Buy Tickets for cinema for film Soul",
//                currentDate,
//                0,
//                0,
//                0
//            )
//            taskDao.insert(task1)
//
//            var task2 = TaskEntity(
//                2,
//                "Do Project",
//                "DO team project for OOP subject",
//                "",
//                0,
//                0,
//                0
//            )
//            taskDao.insert(task2)
//
//            var task3 = TaskEntity(
//                3,
//                "Wash car",
//                "Wash my car",
//                "",
//                0,
//                0,
//                0
//            )
//            taskDao.insert(task3)
//
//            var task4 = TaskEntity(
//                    4,
//                    "Wash T-Shirt",
//                    "Wash my best T-Shirt",
//                    "",
//                    0,
//                    0,
//                    4
//            )
//            taskDao.insert(task4)
//
//            var catalog1 = CatalogEntity(
//                id = 4,
//                name = "FirstCatalog"
//            )
//
//            catalogDao.insert(catalog1)
//        }
//    }
}