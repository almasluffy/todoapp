package com.example.data.ToDoDatabase.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.data.ToDoDatabase.entity.TaskEntity
import com.example.domain.model.TaskModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow

@Dao
interface TaskDao {

    @Query("SELECT * FROM tasks where catalogID=:catalogID")
    suspend fun getTaskList(catalogID: Long): List<TaskEntity>

    @Query("SELECT * FROM tasks")
    suspend fun getAllTasks(): List<TaskEntity>

    @Query("SELECT * FROM tasks where dueDate=:dueDate")
    suspend fun getTodayTaskList(dueDate: String): List<TaskEntity>

    @Query("SELECT * FROM tasks where favorite=1")
    suspend fun getFavoriteTaskList(): List<TaskEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(task: TaskEntity)

    @Query("DELETE FROM tasks WHERE id=:id")
    suspend fun deleteById(id: Long)

    @Query("DELETE FROM tasks")
    suspend fun deleteAll()

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(task: TaskEntity)

    @Query("SELECT * FROM tasks WHERE id=:id")
    suspend fun getTask(id: Long): TaskEntity

    @Query("SELECT id FROM tasks ORDER BY id DESC LIMIT 1")
    suspend fun getMaxTaskID(): Long

    @Query("DELETE FROM tasks where catalogID=:id")
    suspend fun deleteAllCatalogTasks(id: Long)
}