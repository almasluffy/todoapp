package com.example.data.ToDoDatabase.dao

import androidx.room.*
import com.example.data.ToDoDatabase.entity.CatalogEntity

@Dao
interface CatalogDao {

    @Query("SELECT * FROM catalogs")
    suspend fun getCatalogList(): List<CatalogEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(catalog: CatalogEntity)

    @Query("DELETE FROM catalogs")
    suspend fun deleteAll()

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(catalog: CatalogEntity)

    @Delete
    suspend fun delete(catalog: CatalogEntity)

    @Query("SELECT * FROM catalogs WHERE id=:id")
    suspend fun getCatalog(id: Long): CatalogEntity

    @Query("SELECT id FROM catalogs ORDER BY id DESC LIMIT 1")
    suspend fun getMaxCatalogID(): Long
}