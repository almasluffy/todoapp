package com.example.data.ToDoDatabase.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.domain.model.TaskModel

@Entity(tableName = "tasks")
data class TaskEntity (


    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,

    @ColumnInfo(name = "title")
    val title: String,

    @ColumnInfo(name = "description")
    val description: String,

    @ColumnInfo(name = "dueDate")
    val dueDate: String,

    @ColumnInfo(name = "done")
    val done: Int,

    @ColumnInfo(name = "favorite")
    val favorite: Int,

    @ColumnInfo(name = "catalogID")
    val catalogID: Long

)


