package com.example.data

import com.example.data.ToDoDatabase.ToDoRoomDatabase
import com.example.data.repository.CatalogRepositoryImpl
import com.example.data.repository.TaskRepositoryImpl
import com.example.domain.repository.CatalogRepository
import com.example.domain.repository.TaskRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val repositoryModule = module {
    single<TaskRepository>(createdAtStart = false) {
        TaskRepositoryImpl(get(), get())
    }

    single<CatalogRepository>(createdAtStart = false) {
        CatalogRepositoryImpl(get(), get())
    }
}

val roomModule = module {
    single { ToDoRoomDatabase.getDatabase(
        context = androidApplication(),
        scope = get()
    ) }

    single(createdAtStart = false) { get<ToDoRoomDatabase>().taskDao() }
    single(createdAtStart = false) { get<ToDoRoomDatabase>().catalogDao() }

    factory { SupervisorJob() }
    factory { CoroutineScope(Dispatchers.IO) }
}